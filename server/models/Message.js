const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({
    name: {
        type: String,
        default: 'anonymous'
    },
    message: {
        type: String,
        default: '',
        require: true
    },
    type: {
        type: Number,
        default: 0
    },
    createAt:{
        type: Date,
        default: Date.now()
    }
});

var Message = mongoose.model('Message', messageSchema);
module.exports = Message