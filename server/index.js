const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()
const server = require('http').Server(app);
const io = require('socket.io')(server);
const mongoose = require('mongoose');
var Multer  = require('multer');
var path = require("path");
var uuid = require("uuid");
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(jsonParser);
app.use(urlencodedParser);

const Message = require('./models/Message');
mongoose.connect('mongodb://localhost:27017/nuxt-chat', {useNewUrlParser: true});
// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')
io.on('connection', (socket) => {
  console.log(socket.id);
  socket.join('chatroom');
  socket.on('authentication', (data) => {
    console.log(data);
  })
  socket.on('new_message', (data) => {
    console.log(data);
    Message.create(data);
    socket.to('chatroom').emit('new_message', data);
  })
})

app.get('/hello', (req, res) => {
  let data = {
    name : 'admin',
    message: 'Hello from admin'
  }
  io.in('chatroom').emit('new_message', data);
  res.json({
    success: true
  })
})


var multipartUpload = Multer({storage: Multer.diskStorage({
  destination: function (req, file, callback) { callback(null, 'static/uploads');},
  filename: function (req, file, callback) { callback(null, uuid.v4() + path.extname(file.originalname));}})
}).single('image');
app.post('/api/upload', multipartUpload, function (req, res, next) {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were any

  res.json({
    success: true,
    data: {
      filename: '/uploads/' + req.file.filename
    }
  })
})

app.post('/api/login', (req, res) => {
  let token = req.body.token;
  if (token == '123456789') {
    return res.json({
      success: true,
      mesasge: 'Login success',
      data: {
        token: 'this.is.token'
      }
    })
  }

  return res.status(401).json({
    success: false,
    mesasge: 'Login false',
  })
})

app.get('/api/me', (req, res) => {
  res.json({
    success: true,
    data: {
      name: 'HUYEN ND',
      email: 'zangquyet@gmail.com'
    }
  })
})

app.get('/api/messages', async (req, res) =>{
  let data = await Message.find();
  res.json({
    success: true,
    message: '',
    data: data
  })
})




async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  server.listen(port)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
